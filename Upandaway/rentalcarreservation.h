#ifndef RENTALCARRESERVATION_H
#define RENTALCARRESERVATION_H
#include <iostream>
using namespace std;

class RentalCarReservation
{
private:
    long id;
    double price;
    string fromDate;
    string toDate;
    string pickupLocation;
    string returnLocation;
    string company;

public:
    RentalCarReservation();
    void set_id(long id);
    void set_price(double price);
    long get_id(void);
    double get_price(void);
    void set_fromDate(string fromDate);
    string get_fromDate(void);
    void set_toDate(string toDate);
    string get_toDate(void);
    void set_pickupLocation(string pickupLocation);
    string get_pickupLocation(void);
    void set_returnLocation(string returnLocation);
    string get_retunLocation(void);
    void set_company(string company);
    string get_company(void);
    void browse(void);
};

#endif // RENTALCARRESERVATION_H
