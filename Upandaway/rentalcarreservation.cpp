#include "rentalcarreservation.h"

RentalCarReservation::RentalCarReservation()
{

}

void RentalCarReservation::set_id(long id)
{
    this->id = id;
}

void RentalCarReservation::set_price(double price)
{
    this->price = price;
}

long RentalCarReservation::get_id()
{
    return this->id;
}

double RentalCarReservation::get_price()
{
    return this->price;
}

void RentalCarReservation::set_fromDate(string fromDate)
{
    this->fromDate = fromDate;
}

string RentalCarReservation::get_fromDate()
{
    return this->fromDate;
}

void RentalCarReservation::set_toDate(string toDate)
{
    this->toDate = toDate;
}

string RentalCarReservation::get_toDate()
{
    return this->toDate;
}

void RentalCarReservation::set_pickupLocation(string pickupLocation)
{
    this->pickupLocation = pickupLocation;
}

string RentalCarReservation::get_pickupLocation()
{
    return this->pickupLocation;
}

void RentalCarReservation::set_returnLocation(string returnLocation)
{
    this->returnLocation = returnLocation;
}

string RentalCarReservation::get_retunLocation()
{
    return this->returnLocation;
}

void RentalCarReservation::set_company(string company)
{
    this->company = company;
}

string RentalCarReservation::get_company()
{
    return this->company;
}

void RentalCarReservation::browse()
{
    cout << "----RentalCarReservation----" << endl;
    cout << "ID: " << this->get_id() << endl;
    cout << "Price: " << this->get_price() << endl;
    cout << "toDate: " << this->get_toDate() << endl;
    cout << "pickupLocation: " << this->get_pickupLocation() << endl;
    cout << "returnLocation: " << this->get_retunLocation() << endl;
    cout << "company: " << this->get_company() << endl;
}
