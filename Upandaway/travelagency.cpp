#include "travelagency.h"

TravelAgency::TravelAgency()
{
    this->gesamtWert = 0.0;
}

void TravelAgency::readFile(void)
{
    ifstream datei;
    datei.open("bookings.txt",ios::in); //Textdatei muss da wo debug ist
    if(!datei)
    {
        cerr << "Datei kann nicht geoeffnet werden!" << endl;
        exit(-1);
    }

    int i = 0;
    string zeile = "";
    string attribut = "";
    char typ;
    while(!datei.eof())
    {
        getline(datei,zeile);
        stringstream zeilenstream;
        zeilenstream << zeile;
        getline(zeilenstream,attribut, '|');

        typ = attribut[0];
        if(typ == 'F') {
            FlightBooking* flight = new FlightBooking;
    while(!zeilenstream.eof())
    {
        switch(i)
        {

        case 0:
            getline(zeilenstream, attribut, '|');
            flight->set_id(stol(attribut));
            break;
        case 1:
            getline(zeilenstream, attribut, '|');
            flight->set_price(atof(attribut.c_str()));
            break;
        case 2:
            getline(zeilenstream, attribut, '|');
            flight->set_fromDate(attribut);
            break;
        case 3:
            getline(zeilenstream, attribut, '|');
            flight->set_toDate(attribut);
            break;
        case 4:
            getline(zeilenstream, attribut, '|');
            flight->set_fromDest(attribut);
            break;
        case 5:
            getline(zeilenstream, attribut, '|');
            flight->set_toDest(attribut);

            break;
        case 6:
            getline(zeilenstream, attribut, '|');
            flight->set_airline(attribut);
            break;
        default:;
        } //switch
        i++;
    } //while
        i =  0;
        this->flightBookings.push_back(flight);
        } //if
        else if (typ == 'R')
        {
            RentalCarReservation* rental = new RentalCarReservation;
            while(!zeilenstream.eof())
            {
                switch(i)
                {

                case 0:
                    getline(zeilenstream, attribut, '|');
                    rental->set_id(stol(attribut));
                    break;
                case 1:
                    getline(zeilenstream, attribut, '|');
                    rental->set_price(atof(attribut.c_str()));
                    break;
                case 2:
                    getline(zeilenstream, attribut, '|');
                    rental->set_fromDate(attribut);
                    break;
                case 3:
                    getline(zeilenstream, attribut, '|');
                    rental->set_toDate(attribut);
                    break;
                case 4:
                    getline(zeilenstream, attribut, '|');
                    rental->set_pickupLocation(attribut);
                    break;
                case 5:
                    getline(zeilenstream, attribut, '|');
                    rental->set_returnLocation(attribut);
                    break;
                case 6:
                    getline(zeilenstream, attribut, '|');
                    rental->set_company(attribut);
                    break;
                default:;
                } //switch
                i++;
            } //while
                i = 0;
                this->rentalCarReservations.push_back(rental);

        } //else if

        else if (typ == 'H')
        {
              HotelBooking* hotel = new HotelBooking;
            while(!zeilenstream.eof())
            {
                switch(i)
                {

                case 0:
                    getline(zeilenstream, attribut, '|');
                    hotel->set_id(stol(attribut));
                    break;
                case 1:
                    getline(zeilenstream, attribut, '|');
                    hotel->set_price(atof(attribut.c_str()));
                    break;
                case 2:
                    getline(zeilenstream, attribut, '|');
                    hotel->set_fromDate(attribut);
                    break;
                case 3:
                    getline(zeilenstream, attribut, '|');
                    hotel->set_toDate(attribut);
                    break;
                case 4:
                    getline(zeilenstream, attribut, '|');
                    hotel->set_hotel(attribut);
                    break;
                case 5:
                    getline(zeilenstream, attribut, '|');
                    hotel->set_town(attribut);
                    break;
                default:;
                } //switch
                i++;
            } //while
                i = 0;
                this->hotelBookings.push_back(hotel);

        } //else if



    }

    datei.close();
}

double TravelAgency::berechne_gesamtWert()
{
    double summe_fluege = 0.0, summe_hotels = 0.0, summe_cars = 0.0;

    for (unsigned int i = 0; i < this->flightBookings.size(); i++)
    {
        summe_fluege += this->flightBookings.at(i)->get_price();
    }

    for (unsigned int x = 0; x < this->hotelBookings.size(); x++)
    {
        summe_hotels += this->hotelBookings.at(x)->get_price();
    }

    for (unsigned int y = 0; y < this->rentalCarReservations.size(); y++)
    {
        summe_cars += this->rentalCarReservations.at(y)->get_price();
    }

   return this->gesamtWert = summe_fluege + summe_hotels + summe_cars;
}

void TravelAgency::browse()
{
    cout << "Es wurden " << this->flightBookings.size() << " Flugreservierungen, " << this->hotelBookings.size()
    << " Hotelbuchungen und " << this->rentalCarReservations.size() << " Mietwagenreservierungen im gesamt Wert von "
    << this->berechne_gesamtWert() << "$ eingelesen." << endl;

   /* for(unsigned int i = 0; i < this->flightBookings.size();i++)
     {
         this->flightBookings.at(i)->browse();
     }

    for(unsigned int x = 0; x < this->hotelBookings.size();x++)
     {
         this->hotelBookings.at(x)->browse();
     }

    for(unsigned int y = 0; y < this->rentalCarReservations.size();y++)
     {
         this->rentalCarReservations.at(y)->browse();
     } */

}
