#include "hotelbooking.h"

HotelBooking::HotelBooking()
{

}

void HotelBooking::set_id(long id)
{
    this->id = id;
}

long HotelBooking::get_id()
{
    return this->id;
}

void HotelBooking::set_price(double price)
{
    this->price = price;
}

double HotelBooking::get_price()
{
    return this->price;
}

void HotelBooking::set_fromDate(string fromDate)
{
    this->fromDate = fromDate;
}

string HotelBooking::get_fromDate()
{
    return this->fromDate;
}

void HotelBooking::set_toDate(string toDate)
{
    this->toDate = toDate;
}

string HotelBooking::get_toDate()
{
    return this->toDate;
}

void HotelBooking::set_hotel(string hotel)
{
    this->hotel = hotel;
}

string HotelBooking::get_hotel()
{
    return this->hotel;
}

void HotelBooking::set_town(string town)
{
    this->town = town;
}

string HotelBooking::get_town()
{
    return this->town;
}

void HotelBooking::browse()
{
    cout << "----HotelBooking----" << endl;
    cout << "ID: " << this->get_id() << endl;
    cout << "Price: " << this->get_price() << endl;
    cout << "fromDate: " << this->get_fromDate() << endl;
    cout << "toDate: " << this->get_toDate() << endl;
    cout << "hotel: " << this->get_hotel() << endl;
    cout << "town: " << this->get_town() << endl;
}
