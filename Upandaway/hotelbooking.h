#ifndef HOTELBOOKING_H
#define HOTELBOOKING_H
#include <iostream>

using namespace std;

class HotelBooking
{
private:
    long id;
    double price;
    string fromDate;
    string toDate;
    string hotel;
    string town;

public:
    HotelBooking();
    void set_id(long id);
    long get_id(void);
    void set_price(double price);
    double get_price(void);
    void set_fromDate(string fromDate);
    string get_fromDate(void);
    void set_toDate(string toDate);
    string get_toDate(void);
    void set_hotel(string hotel);
    string get_hotel(void);
    void set_town(string town);
    string get_town(void);
    void browse(void);
};

#endif // HOTELBOOKING_H
