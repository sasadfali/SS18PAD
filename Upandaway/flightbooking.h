#ifndef FLIGHTBOOKING_H
#define FLIGHTBOOKING_H
#include <iostream>

using namespace std;
class FlightBooking
{
private:
    long id;
    double price;
    string fromDate;
    string toDate;
    string fromDest;
    string toDest;
    string airline;
public:
    FlightBooking();
    void set_id(long id);
    long get_id(void);
    void set_price(double price);
    double get_price(void);
    void set_fromDate(string fromDate);
    string get_fromDate(void);
    void set_toDate(string toDate);
    string get_toDate(void);
    void set_fromDest(string fromDest);
    string get_fromDest(void);
    void set_toDest(string toDest);
    string get_toDest(void);
    void set_airline(string airline);
    string get_airline(void);



    void browse(void);
};

#endif // FLIGHTBOOKING_H
