#ifndef TRAVELAGENCY_H
#define TRAVELAGENCY_H
#include <vector>
#include <fstream>
#include <sstream>
#include "rentalcarreservation.h"
#include "hotelbooking.h"
#include "flightbooking.h"

class TravelAgency
{
private:
    vector<RentalCarReservation*> rentalCarReservations;
    vector<HotelBooking*> hotelBookings;
    vector<FlightBooking*> flightBookings;
    double gesamtWert;
public:
    TravelAgency();
    void readFile(void);
    double berechne_gesamtWert(void);
    void browse(void);

};

#endif // TRAVELAGENCY_H
