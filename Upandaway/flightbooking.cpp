#include "flightbooking.h"

FlightBooking::FlightBooking()
{

}

void FlightBooking::set_id(long id)
{
    //long i = atol(id);
    this->id = id;
}



long FlightBooking::get_id()
{
    return this->id;
}

void FlightBooking::set_price(double price)
{
    this->price = price;
}

double FlightBooking::get_price()
{
    return this->price;
}

void FlightBooking::set_fromDate(string fromDate)
{
    this->fromDate = fromDate;
}

string FlightBooking::get_fromDate()
{
    return this->fromDate;
}

void FlightBooking::set_toDate(string toDate)
{
    this->toDate = toDate;
}

string FlightBooking::get_toDate()
{
    return this->toDate;
}

void FlightBooking::set_fromDest(string fromDest)
{
    this->fromDest = fromDest;
}

string FlightBooking::get_fromDest()
{
    return this->fromDest;
}

void FlightBooking::set_toDest(string toDest)
{
    this->toDest = toDest;
}

string FlightBooking::get_toDest()
{
    return this->toDest;
}

void FlightBooking::set_airline(string airline)
{
    this->airline = airline;
}

string FlightBooking::get_airline()
{
    return this->airline;
}

void FlightBooking::browse()
{
    cout << "----FlightBooking----" << endl;
    cout << "ID: " << this->get_id() << endl;
    cout << "Price: " << this->get_price() << endl;
    cout << "fromDate: " << this->get_fromDate() << endl;
    cout << "toDate: " << this->get_toDate() << endl;
    cout << "fromDest: " << this->get_fromDest() << endl;
    cout << "toDest: " << this->get_toDest() << endl;
    cout << "ariline: " << this->get_airline() << endl;

}
